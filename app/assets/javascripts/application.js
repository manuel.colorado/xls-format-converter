// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .
// 

$(document).ready(function(e){      
    $("body").on("click", "#convert", function (e){
        var fileUpload = $("#fileUpload")[0];
        var fileUpload_ml = $("#fileUpload")[0].files[0];
        var filename_to_check  = $("#fileUpload")[0].value.split("\\").pop().split(" ")[1].toLowerCase();
        if (filename_to_check == "mercadolibre" || filename_to_check == "ripley"){    
            var reader = new FileReader();
            reader.readAsText(fileUpload_ml);
            reader.onload = function(event) {
                var csvData = event.target.result;
                var data = Papa.parse(csvData, {headers: false});
                var data =  filename_to_check == "mercadolibre" ?  data.data: data.data;
                var data = JSON.stringify(data)
                console.log(data)
                $('#text').val(data); 
                };
                return;
        }   
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xls|.xlsx)$/;
           // if (regex.test(fileUpload.value.toLowerCase())) {
                if (typeof (FileReader) != "undefined") {
                    var reader = new FileReader();
                    if (reader.readAsBinaryString) {
                        reader.onload = function (e) {
                            ProcessExcel(e.target.result,filename_to_check);
                        };
                        reader.readAsBinaryString(fileUpload.files[0]);
                    } else {
                        //IE Explorer 
                        reader.onload = function (e) {
                            var data = "";
                            // guardo array de enteros a conveniencia de IE explorer
                            var bytes = new Uint8Array(e.target.result);
                            for (var i = 0; i < bytes.byteLength; i++) {
                                data += String.fromCharCode(bytes[i]);
                            }
                            ProcessExcel(data,filename_to_check);
                        };
                        reader.readAsArrayBuffer(fileUpload.files[0]);
                    }
                } else {
                    alert("este navegador no soporta html5.");
                }
           // } else {
        //       alert("sube un archivo excel valido");
       //     }

            function ProcessExcel(data,filename_to_check) {
                //leo data excel
                var workbook = XLSX.read(data, {
                    type: 'binary'
                });
                // parseo el nombre del archivo
                var filename_to_check  = $("#fileUpload")[0].value.split("\\").pop().split(" ")[1].toLowerCase();
                //evaluo que hojq tomar en funcion al nombre del archivo
                switch(filename_to_check){
                    case "falabella":
                        var firstSheet = workbook.SheetNames[0]
                    break;
                    case "groupon":
                        var excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets['Detalle cupones']);
                        var parsing_json = JSON.stringify(excelRows);
                        console.log(parsing_json)
                        $('#text').val(parsing_json);
                        return;
                    break;
                    case "mercadolibre":
                        var firstSheet = workbook.SheetNames[2]
                    case "ripley" :
                         var firstSheet = workbook.SheetNames[2]
                    default:
                        alert("ingresa un archivo con formato valido");
                }
                //lee las filas y lo convierte en un array de json
                var excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[firstSheet]);
                var parsing_json = JSON.stringify(excelRows)
                console.log(parsing_json)
                $("#text").val(parsing_json)   
            };     
            return
    });
});
