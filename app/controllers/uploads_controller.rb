class UploadsController < ApplicationController  
    def new 
        @file = Upload.new 
    end 

    def create    
        @code = get_id_liquidation
        # evaliuo el nombre del archivo para user template adecuado
        case name_of_file
        when "falabella" 
            @arr =  JSON.parse(params[:upload][:updated_at])   
            @date = get_date
            render xlsx: 'products', template: 'report/stock_status_report.xlsx.axlsx', filename: "reporte_falabella-#{get_id_liquidation}-#{Date.today}.xlsx", disposition: 'inline',
            lsx_created_at: 3.days.ago
        when "groupon"
            @arr =  JSON.parse(params[:upload][:updated_at])
            @date = get_date
            render xlsx: 'products', template: 'report/reporte_groupon.xlsx.axlsx', filename: "reporte_groupon-#{@arr.first["Liquidacion"]}/#{@arr.last["Liquidacion"]}-#{Date.today}.xlsx", disposition: 'inline',
            lsx_created_at: 3.days.ago
        when "mercadolibre"
            @arr =  JSON.parse(params[:upload][:updated_at])
            @date = get_date
            render xlsx: 'products', template: 'report/reporte_ml.xlsx.axlsx', filename: "reporte_mercadolibre-#{get_id_liquidation}-#{Date.today}.xlsx", disposition: 'inline',
            lsx_created_at: 3.days.ago
        when "ripley"
            @arr =  JSON.parse(params[:upload][:updated_at])
            @date = get_date
            render xlsx: 'products', template: 'report/reporte_ripley.xlsx.axlsx', filename: "reporte_ripley-#{get_id_liquidation}-#{Date.today}.xlsx", disposition: 'inline',
            lsx_created_at: 3.days.ago
        else
            flash.now[:error] = "el archivo que subiste no tiene el formato valido"
        end
    end

    def name_of_file 
        params[:upload][:created_at].original_filename.split[1].downcase
    end

    def get_id_liquidation 
        params[:upload][:created_at].original_filename.split[2]
    end 

    def get_date
        case name_of_file
        when "falabella"
            temp =  params[:upload][:created_at].original_filename.split[3]
        when "groupon"
            temp =  params[:upload][:created_at].original_filename.split[2]
        when "mercadolibre"
            temp =  params[:upload][:created_at].original_filename.split[2]
        when "ripley"
            temp =  params[:upload][:created_at].original_filename.split[3]
        end
       temp = temp.split('.')
       date = ["","",""] 
       temp[0].split("").each_with_index do |item, v |
        if (v < 4)
            date[0] += item 
        elsif (v < 6) 
            date[1] += item
        else
            date[2] += item 
        end
       end 
       date = "#{date[0]}/#{date[1]}/#{date[2]}"
    end 
end
