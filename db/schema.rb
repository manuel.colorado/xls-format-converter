# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190626171553) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "activity", force: :cascade do |t|
    t.string   "hour",             limit: 6
    t.string   "description",      limit: 170, null: false
    t.string   "detail",           limit: 250
    t.string   "user_responsable", limit: 30
    t.integer  "x_base"
    t.integer  "y_variable"
    t.datetime "date_created"
    t.integer  "channel_id"
    t.index ["channel_id"], name: "activity_channel_id_03d01bf6", using: :btree
  end

  create_table "answer", force: :cascade do |t|
    t.text     "answer",                    null: false
    t.bigint   "mercadolibre_id_answer"
    t.datetime "date_answer",               null: false
    t.integer  "question_id",               null: false
    t.integer  "user_id"
    t.datetime "date_created_mercadolibre"
    t.boolean  "is_robot",                  null: false
    t.index ["question_id"], name: "answer_question_id_key", unique: true, using: :btree
    t.index ["user_id"], name: "answer_user_id_5d3bcab5", using: :btree
  end

  create_table "auth_group", force: :cascade do |t|
    t.string "name", limit: 80, null: false
    t.index "name varchar_pattern_ops", name: "auth_group_name_a6ea08ec_like", using: :btree
    t.index ["name"], name: "auth_group_name_key", unique: true, using: :btree
  end

  create_table "auth_group_permissions", force: :cascade do |t|
    t.integer "group_id",      null: false
    t.integer "permission_id", null: false
    t.index ["group_id", "permission_id"], name: "auth_group_permissions_group_id_permission_id_0cd325b0_uniq", unique: true, using: :btree
    t.index ["group_id"], name: "auth_group_permissions_group_id_b120cbf9", using: :btree
    t.index ["permission_id"], name: "auth_group_permissions_permission_id_84c5c92e", using: :btree
  end

  create_table "auth_permission", force: :cascade do |t|
    t.string  "name",            limit: 255, null: false
    t.integer "content_type_id",             null: false
    t.string  "codename",        limit: 100, null: false
    t.index ["content_type_id", "codename"], name: "auth_permission_content_type_id_codename_01ab375a_uniq", unique: true, using: :btree
    t.index ["content_type_id"], name: "auth_permission_content_type_id_2f476e4b", using: :btree
  end

  create_table "auth_user", force: :cascade do |t|
    t.string   "password",        limit: 128, null: false
    t.datetime "last_login"
    t.boolean  "is_superuser",                null: false
    t.string   "username",        limit: 150, null: false
    t.string   "first_name",      limit: 30,  null: false
    t.string   "last_name",       limit: 150, null: false
    t.string   "email",           limit: 254, null: false
    t.boolean  "is_staff",                    null: false
    t.boolean  "is_active",                   null: false
    t.datetime "date_joined",                 null: false
    t.string   "rut",             limit: 15,  null: false
    t.string   "phone",           limit: 20
    t.string   "web",             limit: 255
    t.string   "address",         limit: 255
    t.string   "user_status",     limit: 10,  null: false
    t.text     "review",                      null: false
    t.datetime "superuser_until"
    t.index "rut varchar_pattern_ops", name: "auth_user_rut_bf6344c4_like", using: :btree
    t.index "username varchar_pattern_ops", name: "auth_user_username_6821ab7c_like", using: :btree
    t.index ["rut"], name: "auth_user_rut_key", unique: true, using: :btree
    t.index ["username"], name: "auth_user_username_key", unique: true, using: :btree
  end

  create_table "auth_user_groups", force: :cascade do |t|
    t.integer "user_id",  null: false
    t.integer "group_id", null: false
    t.index ["group_id"], name: "auth_user_groups_group_id_97559544", using: :btree
    t.index ["user_id", "group_id"], name: "auth_user_groups_user_id_group_id_94350c0c_uniq", unique: true, using: :btree
    t.index ["user_id"], name: "auth_user_groups_user_id_6a12ed8b", using: :btree
  end

  create_table "auth_user_user_permissions", force: :cascade do |t|
    t.integer "user_id",       null: false
    t.integer "permission_id", null: false
    t.index ["permission_id"], name: "auth_user_user_permissions_permission_id_1fbb5f2c", using: :btree
    t.index ["user_id", "permission_id"], name: "auth_user_user_permissions_user_id_permission_id_14a6b632_uniq", unique: true, using: :btree
    t.index ["user_id"], name: "auth_user_user_permissions_user_id_a95ead1b", using: :btree
  end

  create_table "automatic_message", id: :integer, default: -> { "nextval('notifications_automaticmessage_id_seq'::regclass)" }, force: :cascade do |t|
    t.text   "message"
    t.string "message_type", limit: 20
  end

  create_table "bank-report", force: :cascade do |t|
    t.string   "bank_register",   limit: 10,                           null: false
    t.string   "description",     limit: 200,                          null: false
    t.bigint   "document_number"
    t.decimal  "total_amount",                precision: 10, scale: 1
    t.string   "detail",          limit: 50
    t.date     "date_report"
    t.datetime "date_created"
  end

  create_table "branch_office", force: :cascade do |t|
    t.string  "name",     limit: 100
    t.string  "address",  limit: 150
    t.string  "phone",    limit: 20
    t.boolean "santiago",             null: false
    t.integer "order"
  end

  create_table "category", id: :integer, default: -> { "nextval('categories_category_id_seq'::regclass)" }, force: :cascade do |t|
    t.string  "name",        limit: 150, null: false
    t.string  "code",        limit: 25,  null: false
    t.integer "total_items",             null: false
    t.integer "channel_id",              null: false
    t.index ["channel_id"], name: "categories_category_channel_id_7f77f935", using: :btree
  end

  create_table "category_product_sale", force: :cascade do |t|
    t.string   "name",         limit: 50, null: false
    t.string   "sku_start",    limit: 5,  null: false
    t.datetime "date_created"
  end

  create_table "channel", id: :integer, default: -> { "nextval('categories_channel_id_seq'::regclass)" }, force: :cascade do |t|
    t.string "name",     limit: 50,  null: false
    t.string "web_page", limit: 200
  end

  create_table "channel_commission", force: :cascade do |t|
    t.integer "percentage_commission"
    t.integer "channel_id",                       null: false
    t.integer "pack_id"
    t.integer "product_sale_id"
    t.string  "category_sku",          limit: 25
    t.index ["channel_id"], name: "channel_commission_channel_id_53e770e7", using: :btree
    t.index ["pack_id"], name: "channel_commission_pack_id_a89ecfb4", using: :btree
    t.index ["product_sale_id"], name: "channel_commission_product_sale_id_cb45b550", using: :btree
  end

  create_table "claims", force: :cascade do |t|
    t.string "order_channel", limit: 20
    t.string "reference",     limit: 80
    t.string "subject",       limit: 50
    t.string "response",      limit: 250
    t.string "status",        limit: 50
  end

  create_table "clients_returns", force: :cascade do |t|
    t.string "name",  limit: 50
    t.string "email", limit: 254
    t.string "phone", limit: 20
  end

  create_table "color", force: :cascade do |t|
    t.string "description", limit: 100, null: false
  end

  create_table "color_product", force: :cascade do |t|
    t.integer "quantity",   null: false
    t.integer "color_id",   null: false
    t.integer "product_id", null: false
    t.index ["color_id"], name: "color_product_color_id_def2e2d2", using: :btree
    t.index ["product_id"], name: "color_product_product_id_4f51680d", using: :btree
  end

  create_table "courier", force: :cascade do |t|
    t.string "name", limit: 80
    t.string "logo", limit: 100
  end

  create_table "custom_message_product", force: :cascade do |t|
    t.text    "message"
    t.integer "pack_id"
    t.integer "product_id"
    t.index ["pack_id"], name: "custom_message_product_pack_id_889a505c", using: :btree
    t.index ["product_id"], name: "custom_message_product_product_id_key", unique: true, using: :btree
  end

  create_table "dimensions_product_sale", force: :cascade do |t|
    t.decimal "height",  precision: 10, scale: 3
    t.decimal "weight",  precision: 10, scale: 3
    t.decimal "length",  precision: 10, scale: 3
    t.decimal "width",   precision: 10, scale: 3
    t.integer "sale_id"
    t.index ["sale_id"], name: "dimensions_product_sale_sale_id_key", unique: true, using: :btree
  end

  create_table "django_admin_log", force: :cascade do |t|
    t.datetime "action_time",                 null: false
    t.text     "object_id"
    t.string   "object_repr",     limit: 200, null: false
    t.integer  "action_flag",     limit: 2,   null: false
    t.text     "change_message",              null: false
    t.integer  "content_type_id"
    t.integer  "user_id",                     null: false
    t.index ["content_type_id"], name: "django_admin_log_content_type_id_c4bce8eb", using: :btree
    t.index ["user_id"], name: "django_admin_log_user_id_c564eba6", using: :btree
  end

  create_table "django_content_type", force: :cascade do |t|
    t.string "app_label", limit: 100, null: false
    t.string "model",     limit: 100, null: false
    t.index ["app_label", "model"], name: "django_content_type_app_label_model_76bd3d3b_uniq", unique: true, using: :btree
  end

  create_table "django_migrations", force: :cascade do |t|
    t.string   "app",     limit: 255, null: false
    t.string   "name",    limit: 255, null: false
    t.datetime "applied",             null: false
  end

  create_table "django_session", primary_key: "session_key", id: :string, limit: 40, force: :cascade do |t|
    t.text     "session_data", null: false
    t.datetime "expire_date",  null: false
    t.index "session_key varchar_pattern_ops", name: "django_session_session_key_c0390e0f_like", using: :btree
    t.index ["expire_date"], name: "django_session_expire_date_a5c62663", using: :btree
  end

  create_table "inventory", force: :cascade do |t|
    t.string   "name",             limit: 15
    t.text     "description"
    t.datetime "date_created"
    t.integer  "user_charge_id"
    t.datetime "date_stock_break"
    t.index ["user_charge_id"], name: "inventory_user_charge_id_fbd05dbb", using: :btree
  end

  create_table "metrics_mercadolibre", force: :cascade do |t|
    t.integer  "daily_sales_count"
    t.integer  "daily_sales_sum"
    t.decimal  "daily_sales_average",  precision: 8, scale: 3
    t.integer  "publications_days"
    t.decimal  "historic_sales",       precision: 8, scale: 3
    t.integer  "acumulated"
    t.decimal  "average_reward",       precision: 8, scale: 3
    t.decimal  "average_last_28",      precision: 8, scale: 3
    t.decimal  "average_last_7",       precision: 8, scale: 3
    t.decimal  "average_last_14",      precision: 8, scale: 3
    t.decimal  "mode",                 precision: 6, scale: 3
    t.decimal  "median",               precision: 8, scale: 3
    t.integer  "location_by_category"
    t.integer  "competition_number"
    t.integer  "sellers_publications"
    t.integer  "hope_60"
    t.integer  "hope_30"
    t.decimal  "rank",                 precision: 5, scale: 3
    t.datetime "created_at"
    t.integer  "product_id"
    t.integer  "hope_90"
    t.index ["product_id"], name: "metrics_mercadolibre_product_id_key", unique: true, using: :btree
  end

  create_table "notifications_specialevent", force: :cascade do |t|
    t.string   "name",         limit: 200, null: false
    t.datetime "date_created"
    t.integer  "channel_id"
    t.index ["channel_id"], name: "notifications_specialevent_channel_id_e52770ec", using: :btree
  end

  create_table "notifications_video", force: :cascade do |t|
    t.string "name",        limit: 500
    t.string "videofile",   limit: 100
    t.string "description", limit: 500
  end

  create_table "packs", force: :cascade do |t|
    t.string   "sku_pack",         limit: 35
    t.string   "description",      limit: 200,                          null: false
    t.datetime "date_created"
    t.integer  "category_pack_id"
    t.integer  "channel_id"
    t.integer  "price"
    t.decimal  "height",                       precision: 10, scale: 1
    t.decimal  "length",                       precision: 10, scale: 1
    t.decimal  "weight",                       precision: 10, scale: 1
    t.decimal  "width",                        precision: 10, scale: 1
    t.index ["category_pack_id"], name: "packs_category_pack_id_eff90d0d", using: :btree
    t.index ["channel_id"], name: "packs_channel_id_a0f7b149", using: :btree
  end

  create_table "packs_products", force: :cascade do |t|
    t.integer  "quantity",         null: false
    t.integer  "pack_id"
    t.integer  "product_pack_id"
    t.integer  "percentage_price"
    t.datetime "date_created"
    t.integer  "color_id"
    t.index ["color_id"], name: "packs_products_color_id_78cb93c7", using: :btree
    t.index ["pack_id"], name: "packs_products_pack_id_d25cb599", using: :btree
    t.index ["product_pack_id"], name: "packs_products_product_pack_id_e95ec08c", using: :btree
  end

  create_table "price_product", force: :cascade do |t|
    t.integer "normal_price"
    t.integer "wholesaler_price"
    t.integer "falabella_price"
    t.integer "mercadolibre_price"
    t.integer "pack_id"
    t.integer "product_sale_id"
    t.date    "date_end"
    t.date    "date_start"
    t.integer "falabella_offer_price"
    t.index ["pack_id"], name: "price_product_pack_id_key", unique: true, using: :btree
    t.index ["product_sale_id"], name: "price_product_product_sale_id_key", unique: true, using: :btree
  end

  create_table "price_variation_pcfactory", id: :integer, default: -> { "nextval('products_pricevariationpcfactory_id_seq'::regclass)" }, force: :cascade do |t|
    t.date     "date_search"
    t.integer  "price"
    t.integer  "product_id"
    t.datetime "created_at"
    t.index ["product_id"], name: "products_pricevariationpcfactory_product_id_fae74b54", using: :btree
  end

  create_table "product", id: :integer, default: -> { "nextval('products_product_id_seq'::regclass)" }, force: :cascade do |t|
    t.string   "code",                      limit: 80,                           null: false
    t.string   "name",                      limit: 350,                          null: false
    t.decimal  "price",                                 precision: 20, scale: 1
    t.integer  "sold_quantity"
    t.integer  "sub_category_id"
    t.integer  "available_quantity"
    t.decimal  "original_price",                        precision: 20, scale: 1
    t.decimal  "rating",                                precision: 4,  scale: 2
    t.date     "stop_time"
    t.string   "url_product",               limit: 200
    t.decimal  "referencial_price",                     precision: 20, scale: 1
    t.integer  "seller_id"
    t.boolean  "from_seller_list"
    t.integer  "seller_id_ml"
    t.string   "last_category",             limit: 60
    t.integer  "channel_id"
    t.string   "last_category_description", limit: 60
    t.datetime "created_at"
    t.index ["channel_id"], name: "product_channel_id_bdec9f54", using: :btree
    t.index ["seller_id"], name: "product_seller_id_cb2471f5", using: :btree
    t.index ["sub_category_id"], name: "products_product_category_id_9b594869", using: :btree
  end

  create_table "product_branch_office", force: :cascade do |t|
    t.integer  "branch_stock"
    t.integer  "office_id"
    t.integer  "product_id"
    t.date     "date_search"
    t.datetime "created_at"
    t.index ["office_id"], name: "product_branch_office_office_id_ebe00e24", using: :btree
    t.index ["product_id"], name: "product_branch_office_product_id_38f3e686", using: :btree
  end

  create_table "product_channel_pivote", id: :integer, default: -> { "nextval('sales_product_channel_id_seq'::regclass)" }, force: :cascade do |t|
    t.integer  "assigned_amount", null: false
    t.datetime "assignment_date"
    t.datetime "date_update",     null: false
    t.integer  "channel_id"
    t.integer  "product_id"
    t.datetime "date_created"
    t.index ["channel_id"], name: "sales_product_channel_channel_id_b23a12bd", using: :btree
    t.index ["product_id"], name: "sales_product_channel_product_id_90a04182", using: :btree
  end

  create_table "product_sale", force: :cascade do |t|
    t.integer  "sku",                                                           null: false
    t.string   "description",              limit: 140,                          null: false
    t.integer  "stock",                                                         null: false
    t.decimal  "price",                                precision: 10, scale: 1
    t.integer  "category_product_sale_id"
    t.datetime "date_created"
    t.decimal  "height",                               precision: 10, scale: 1
    t.decimal  "length",                               precision: 10, scale: 1
    t.decimal  "weight",                               precision: 10, scale: 1
    t.decimal  "width",                                precision: 10, scale: 1
    t.integer  "box_quantity"
    t.index ["category_product_sale_id"], name: "product_sale_category_product_sale_id_a0f02bd6", using: :btree
  end

  create_table "products_variation", id: :integer, default: -> { "nextval('products_productvariation_id_seq'::regclass)" }, force: :cascade do |t|
    t.integer  "sold_quantity"
    t.date     "date_search",          null: false
    t.integer  "available_quantity"
    t.integer  "product_id",           null: false
    t.integer  "diference_day_before"
    t.datetime "created_at"
    t.integer  "price"
    t.integer  "daily_reward"
    t.boolean  "is_projected"
    t.integer  "projected_sale"
    t.boolean  "is_reused",            null: false
    t.index ["product_id"], name: "products_productvariation_product_id_6a2d04b0", using: :btree
  end

  create_table "provider", force: :cascade do |t|
    t.string "rut",           limit: 30
    t.string "name",          limit: 150, null: false
    t.string "provider_type", limit: 30,  null: false
    t.string "address",       limit: 120
    t.string "phone",         limit: 15
    t.string "email",         limit: 110
  end

  create_table "publications_mercadolibre", id: :integer, default: -> { "nextval('notifications_publicationdatamercadolibre_id_seq'::regclass)" }, force: :cascade do |t|
    t.string  "title",            limit: 250
    t.string  "link_publication", limit: 200
    t.string  "img_first",        limit: 200
    t.string  "mlc",              limit: 15
    t.string  "custom_field",     limit: 10
    t.integer "seller_id"
    t.index ["seller_id"], name: "notifications_publicationdatamercadolibre_seller_id_2019caf8", using: :btree
  end

  create_table "purchase", force: :cascade do |t|
    t.date     "date_purchase",                                       null: false
    t.datetime "date_created",                                        null: false
    t.string   "number_document", limit: 30
    t.decimal  "total_purchase",             precision: 15, scale: 1, null: false
    t.string   "document_type",   limit: 20,                          null: false
    t.string   "purchase_type",   limit: 15,                          null: false
    t.integer  "provider_id"
    t.integer  "user_id"
    t.integer  "warehouse_id"
    t.index ["provider_id"], name: "purchase_provider_id_74018c22", using: :btree
    t.index ["user_id"], name: "purchase_user_id_7b5a7dd4", using: :btree
    t.index ["warehouse_id"], name: "purchase_warehouse_id_f6182a98", using: :btree
  end

  create_table "purchase_product", force: :cascade do |t|
    t.integer "quantity",                                          null: false
    t.integer "seller_sku"
    t.decimal "unit_price",               precision: 15, scale: 1, null: false
    t.decimal "product_purchase_total",   precision: 15, scale: 1, null: false
    t.decimal "credit_iva",               precision: 9,  scale: 2, null: false
    t.integer "minimun_price_bill"
    t.integer "product_purchase_id"
    t.integer "purchase_id"
    t.integer "product_purchase_code_id"
    t.decimal "average_price",            precision: 15, scale: 1
    t.index ["product_purchase_code_id"], name: "purchase_product_product_purchase_code_id_b42e129f", using: :btree
    t.index ["product_purchase_id"], name: "purchase_product_product_purchase_id_a0f8bb9a", using: :btree
    t.index ["purchase_id"], name: "purchase_product_purchase_id_87b8d1e8", using: :btree
  end

  create_table "questions", force: :cascade do |t|
    t.text     "text",                                null: false
    t.boolean  "is_answer",                           null: false
    t.bigint   "mercadolibre_id_question"
    t.bigint   "user_id_ml"
    t.string   "user_nickname_ml",         limit: 40
    t.datetime "date_created",                        null: false
    t.integer  "channel_id"
    t.integer  "publication_data_id"
    t.text     "suggested_answer"
    t.string   "key_words",                limit: 30,              array: true
    t.index ["channel_id"], name: "questions_channel_id_5ab07d06", using: :btree
    t.index ["publication_data_id"], name: "questions_publication_data_id_e9b9a966", using: :btree
  end

  create_table "report_activity", force: :cascade do |t|
    t.boolean  "status",               null: false
    t.integer  "count"
    t.integer  "identified"
    t.integer  "work_time"
    t.datetime "date_created"
    t.integer  "activity_id"
    t.integer  "report_supervisor_id"
    t.index ["activity_id"], name: "report_activity_activity_id_b02f9fc1", using: :btree
    t.index ["report_supervisor_id"], name: "report_activity_report_supervisor_id_750f7c7f", using: :btree
  end

  create_table "report_supervisor", force: :cascade do |t|
    t.string   "file_name",         limit: 60
    t.datetime "date_created"
    t.integer  "user_generated_id"
    t.index ["user_generated_id"], name: "report_supervisor_user_generated_id_c04d592b", using: :btree
  end

  create_table "retail-payment", force: :cascade do |t|
    t.string   "bill_register",          limit: 30, null: false
    t.string   "shipping_bill_number",   limit: 15, null: false
    t.string   "commission_bill_number", limit: 15, null: false
    t.date     "date_payment"
    t.datetime "date_created"
    t.integer  "channel_id",                        null: false
    t.integer  "bank_register_id"
    t.index ["bank_register_id"], name: "retail-payment_bank_register_id_key", unique: true, using: :btree
    t.index ["channel_id"], name: "retail-payment_channel_id_8b1be360", using: :btree
  end

  create_table "retail-payment-detail", force: :cascade do |t|
    t.decimal "bill_mount",                   precision: 10, scale: 1
    t.decimal "partial_mount",                precision: 10, scale: 1
    t.decimal "commission_mount",             precision: 10, scale: 1
    t.decimal "shipping_mount",               precision: 10, scale: 1
    t.decimal "product_mount",                precision: 10, scale: 1
    t.string  "movement_type",     limit: 10,                          null: false
    t.integer "sale_id",                                               null: false
    t.integer "retail_payment_id"
    t.index ["retail_payment_id"], name: "retail-payment-detail_retail_payment_id_c401df39", using: :btree
    t.index ["sale_id"], name: "retail-payment-detail_sale_id_d6c3fa95", using: :btree
  end

  create_table "retirement", force: :cascade do |t|
    t.date     "date_retirement",              null: false
    t.integer  "total_products",               null: false
    t.integer  "document_number"
    t.datetime "date_created"
    t.integer  "user_created_id"
    t.integer  "warehouse_id"
    t.string   "token_bill",        limit: 70
    t.string   "status_retirement", limit: 30
    t.index ["user_created_id"], name: "retirement_user_created_id_88030366", using: :btree
    t.index ["warehouse_id"], name: "retirement_warehouse_id_2aee8750", using: :btree
  end

  create_table "retirement_product", force: :cascade do |t|
    t.integer "quantity",                                 null: false
    t.decimal "unit_price",      precision: 10, scale: 2, null: false
    t.decimal "total",           precision: 10, scale: 2, null: false
    t.integer "product_code_id"
    t.integer "retirement_id",                            null: false
    t.integer "product_sale_id"
    t.index ["product_code_id"], name: "retirement_product_product_code_id_b6ecb438", using: :btree
    t.index ["product_sale_id"], name: "retirement_product_product_sale_id_f9a0816f", using: :btree
    t.index ["retirement_id"], name: "retirement_product_retirement_id_427017b2", using: :btree
  end

  create_table "return", force: :cascade do |t|
    t.date     "admission_date",                       null: false
    t.string   "status_box",                limit: 25
    t.text     "comment"
    t.string   "client_request",            limit: 40
    t.text     "client_comment"
    t.integer  "total_products"
    t.boolean  "bill_receive",                         null: false
    t.date     "limit_date"
    t.datetime "date_created"
    t.integer  "client_id"
    t.integer  "courier_id"
    t.integer  "sale_id"
    t.integer  "user_id"
    t.string   "status_package",            limit: 25
    t.string   "status_product",            limit: 25
    t.boolean  "missing",                              null: false
    t.boolean  "technical_report_generate",            null: false
    t.index ["client_id"], name: "return_client_id_24850826", using: :btree
    t.index ["courier_id"], name: "return_courier_id_201ffc85", using: :btree
    t.index ["sale_id"], name: "return_sale_id_064626bb", using: :btree
    t.index ["user_id"], name: "return_user_id_5fb74454", using: :btree
  end

  create_table "return_product", force: :cascade do |t|
    t.text    "list_accesories"
    t.integer "quantity"
    t.integer "product_sale_return_id"
    t.integer "return_sale_id"
    t.integer "product_sale_code_return_id"
    t.boolean "reintegrate",                 null: false
    t.index ["product_sale_code_return_id"], name: "return_product_product_sale_code_return_id_1ce33ad4", using: :btree
    t.index ["product_sale_return_id"], name: "return_product_product_sale_return_id_e8699c04", using: :btree
    t.index ["return_sale_id"], name: "return_product_return_sale_id_3448c15d", using: :btree
  end

  create_table "robot_aceptance", force: :cascade do |t|
    t.integer  "percentage_aceptance"
    t.datetime "date_created",          null: false
    t.integer  "response_id"
    t.integer  "question_asociated_id"
    t.index ["question_asociated_id"], name: "robot_aceptance_question_asociated_id_key", unique: true, using: :btree
    t.index ["response_id"], name: "robot_aceptance_response_id_4d411e5d", using: :btree
  end

  create_table "robot_intention", force: :cascade do |t|
    t.string   "text_intention", limit: 100
    t.datetime "date_created",               null: false
  end

  create_table "robot_intention_words", force: :cascade do |t|
    t.datetime "date_created", null: false
    t.integer  "intention_id"
    t.integer  "word_id"
    t.index ["intention_id"], name: "robot_intention_words_intention_id_ab92c539", using: :btree
    t.index ["word_id"], name: "robot_intention_words_word_id_4e78e359", using: :btree
  end

  create_table "robot_response", force: :cascade do |t|
    t.string   "positive_answer", limit: 200
    t.string   "negative_answer", limit: 200
    t.datetime "date_created",                null: false
    t.integer  "intention_id"
    t.text     "condition_text"
    t.index ["intention_id"], name: "robot_response_intention_id_key", unique: true, using: :btree
  end

  create_table "robot_synonyms", force: :cascade do |t|
    t.string   "text_synonym", limit: 30
    t.datetime "date_created",            null: false
    t.integer  "word_id"
    t.index ["word_id"], name: "robot_synonyms_word_id_6eceeb3a", using: :btree
  end

  create_table "robot_words", force: :cascade do |t|
    t.string   "text_word",     limit: 30
    t.string   "category_word", limit: 100, null: false
    t.datetime "date_created",              null: false
  end

  create_table "robot_words_publication_asociated", force: :cascade do |t|
    t.integer "word_id",                        null: false
    t.integer "publicationdatamercadolibre_id", null: false
    t.index ["publicationdatamercadolibre_id"], name: "robot_words_publication_as_publicationdatamercadolibr_367337eb", using: :btree
    t.index ["word_id", "publicationdatamercadolibre_id"], name: "robot_words_publication__word_id_publicationdatam_b1cc806e_uniq", unique: true, using: :btree
    t.index ["word_id"], name: "robot_words_publication_asociated_word_id_cafff071", using: :btree
  end

  create_table "robot_wrong_words", force: :cascade do |t|
    t.string   "text_wrong",   limit: 30
    t.datetime "date_created",            null: false
    t.integer  "synonym_id"
    t.integer  "word_id"
    t.index ["synonym_id"], name: "robot_wrong_words_synonym_id_e893c985", using: :btree
    t.index ["word_id"], name: "robot_wrong_words_word_id_5689dac0", using: :btree
  end

  create_table "sale_product_commission", force: :cascade do |t|
    t.decimal  "api_commission",          precision: 10, scale: 3
    t.decimal  "calculate_commission",    precision: 10, scale: 3
    t.decimal  "discount_commission",     precision: 10, scale: 3
    t.datetime "date_created",                                     null: false
    t.integer  "sale_product_id",                                  null: false
    t.decimal  "shipping_price_assigned", precision: 10, scale: 3
    t.index ["sale_product_id"], name: "sale_product_commission_sale_product_id_key", unique: true, using: :btree
  end

  create_table "sale_product_pivote", force: :cascade do |t|
    t.integer  "quantity",                                             null: false
    t.decimal  "unit_price",                  precision: 10, scale: 2, null: false
    t.decimal  "total",                       precision: 10, scale: 2, null: false
    t.integer  "product_id"
    t.integer  "sale_id"
    t.integer  "product_code_id"
    t.datetime "date_created"
    t.integer  "from_pack_id"
    t.string   "comment_unit",    limit: 350
    t.index ["from_pack_id"], name: "sale_product_pivote_from_pack_id_c09e8413", using: :btree
    t.index ["product_code_id"], name: "sale_product_pivote_product_code_id_2408f0ac", using: :btree
    t.index ["product_id"], name: "sale_product_pivote_product_id_61d605ea", using: :btree
    t.index ["sale_id"], name: "sale_product_pivote_sale_id_d82bb353", using: :btree
  end

  create_table "sales", force: :cascade do |t|
    t.bigint   "order_channel"
    t.integer  "total_sale"
    t.string   "document_type",            limit: 40
    t.integer  "number_document"
    t.string   "payment_type",             limit: 40, null: false
    t.text     "comments"
    t.string   "sender_responsable",       limit: 40, null: false
    t.string   "status",                   limit: 40, null: false
    t.string   "status_product",           limit: 40, null: false
    t.date     "date_sale",                           null: false
    t.integer  "channel_id"
    t.integer  "user_id"
    t.datetime "date_update",                         null: false
    t.datetime "date_created"
    t.boolean  "bill",                                null: false
    t.string   "token_bill",               limit: 70
    t.integer  "declared_total"
    t.string   "channel_status",           limit: 40
    t.string   "hour_sale_channel",        limit: 6
    t.integer  "revenue"
    t.bigint   "tracking_number"
    t.string   "shipping_type",            limit: 15
    t.string   "shipping_status",          limit: 25
    t.string   "shipping_type_sub_status", limit: 30
    t.integer  "last_user_update_id"
    t.integer  "total_number_of_product"
    t.string   "cut_inventory",            limit: 10
    t.string   "mlc",                      limit: 14
    t.bigint   "user_id_ml"
    t.date     "shipping_date"
    t.integer  "credit_note_number"
    t.integer  "sales_report_id"
    t.date     "date_document_emision"
    t.integer  "shipping_guide_number"
    t.index ["channel_id"], name: "sales_channel_id_bd881a60", using: :btree
    t.index ["last_user_update_id"], name: "sales_last_user_update_id_81028058", using: :btree
    t.index ["sales_report_id"], name: "sales_sales_report_id_c0db2bff", using: :btree
    t.index ["user_id"], name: "sales_user_id_894ff46a", using: :btree
  end

  create_table "sales_orders_email", force: :cascade do |t|
    t.string "email", limit: 40
  end

  create_table "sales_product_code", force: :cascade do |t|
    t.string   "code_seven_digits", limit: 7
    t.integer  "color_id"
    t.integer  "product_id"
    t.integer  "quantity"
    t.datetime "date_created"
    t.datetime "date_reset"
    t.boolean  "reset",                       null: false
    t.integer  "quantity_reset"
    t.integer  "user_reset_id"
    t.integer  "merma"
    t.index ["color_id"], name: "sales_product_code_color_id_11d79572", using: :btree
    t.index ["product_id"], name: "sales_product_code_product_id_0aad3e9b", using: :btree
    t.index ["user_reset_id"], name: "sales_product_code_user_reset_id_417bc1c1", using: :btree
  end

  create_table "sales_report", id: :integer, default: -> { "nextval('sales_sales_report_id_seq'::regclass)" }, force: :cascade do |t|
    t.string   "report_file_name", limit: 60
    t.datetime "date_created"
    t.integer  "user_created_id"
    t.index ["user_created_id"], name: "sales_sales_report_user_created_id_99bc84fb", using: :btree
  end

  create_table "seller", id: :integer, default: -> { "nextval('sellers_seller_id_seq'::regclass)" }, force: :cascade do |t|
    t.string  "nickname",            limit: 40,                          null: false
    t.date    "registration_date",                                       null: false
    t.string  "city",                limit: 40
    t.string  "user_type",           limit: 20,                          null: false
    t.string  "tags",                limit: 60,                          null: false
    t.integer "points",                                                  null: false
    t.string  "site_id",             limit: 10,                          null: false
    t.string  "seller_url",          limit: 200
    t.string  "level_ml",            limit: 30
    t.string  "power_seller_status", limit: 30
    t.integer "complete_sales",                                          null: false
    t.integer "canceled_sales",                                          null: false
    t.integer "total_sales",                                             null: false
    t.decimal "negative_percent",                precision: 4, scale: 1, null: false
    t.decimal "neutral_percent",                 precision: 4, scale: 1, null: false
    t.decimal "positive_percent",                precision: 4, scale: 1, null: false
    t.string  "user_status",         limit: 15,                          null: false
    t.integer "id_ml"
    t.integer "number_of_items",                                         null: false
  end

  create_table "seller_variation", force: :cascade do |t|
    t.date     "date_search",        null: false
    t.integer  "sales_last_4_month"
    t.datetime "created_at",         null: false
    t.integer  "seller_id"
    t.integer  "daily_sales",        null: false
    t.index ["seller_id"], name: "seller_variation_seller_id_74d3276c", using: :btree
  end

  create_table "sku_category_channel", force: :cascade do |t|
    t.datetime "date_created",            null: false
    t.integer  "pack_id"
    t.integer  "product_sale_id"
    t.integer  "sub_category_channel_id"
    t.index ["pack_id"], name: "sku_category_channel_pack_id_0dd2d37f", using: :btree
    t.index ["product_sale_id"], name: "sku_category_channel_product_sale_id_68897523", using: :btree
    t.index ["sub_category_channel_id"], name: "sku_category_channel_sub_category_channel_id_4c11998f", using: :btree
  end

  create_table "sku_ripley", force: :cascade do |t|
    t.string  "description",       limit: 200
    t.string  "sku_ripley",        limit: 35
    t.integer "color_id"
    t.integer "pack_ripley_id"
    t.string  "code",              limit: 30
    t.integer "channel_id"
    t.integer "product_ripley_id"
    t.string  "code_seven_digits", limit: 7
    t.index ["channel_id"], name: "sku_ripley_channel_id_3cc765c8", using: :btree
    t.index ["color_id"], name: "sku_ripley_color_id_b0ce3c2b", using: :btree
    t.index ["pack_ripley_id"], name: "sku_ripley_pack_ripley_id_70c41e19", using: :btree
    t.index ["product_ripley_id"], name: "sku_ripley_product_ripley_id_34f57b91", using: :btree
  end

  create_table "stock_inventory_product", force: :cascade do |t|
    t.integer  "stock_good"
    t.integer  "stock_bad"
    t.integer  "stock_detail"
    t.integer  "total_stock"
    t.datetime "date_created"
    t.datetime "date_updated",     null: false
    t.integer  "inventory_id",     null: false
    t.integer  "product_id"
    t.integer  "product_code_id"
    t.integer  "reaconditionated"
    t.index ["inventory_id"], name: "stock_inventory_product_inventory_id_5e4fcc86", using: :btree
    t.index ["product_code_id"], name: "stock_inventory_product_product_code_id_3ca4eefb", using: :btree
    t.index ["product_id"], name: "stock_inventory_product_product_id_005ebb2a", using: :btree
  end

  create_table "stock_warehouse", force: :cascade do |t|
    t.integer  "initial_stock"
    t.datetime "date_count"
    t.integer  "warehouse_id",    null: false
    t.integer  "product_sale_id"
    t.index ["product_sale_id"], name: "stock_warehouse_product_sale_id_10d4ce65", using: :btree
    t.index ["warehouse_id"], name: "stock_warehouse_warehouse_id_2e72d93a", using: :btree
  end

  create_table "students", force: :cascade do |t|
    t.string   "fname"
    t.string   "lname"
    t.integer  "marks"
    t.float    "percentage"
    t.string   "grade"
    t.string   "remark"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sub-categories", id: :integer, default: -> { "nextval('categories_subcategories_id_seq'::regclass)" }, force: :cascade do |t|
    t.string  "name",             limit: 200, null: false
    t.string  "code",             limit: 25,  null: false
    t.integer "total_items",                  null: false
    t.integer "category_id",                  null: false
    t.string  "url_sub_category", limit: 200
    t.index ["category_id"], name: "categories_subcategories_category_id_5056b671", using: :btree
  end

  create_table "sub_category_channel", force: :cascade do |t|
    t.integer  "commission"
    t.string   "name",                   limit: 200, null: false
    t.datetime "date_created",                       null: false
    t.integer  "category_asiamerica_id",             null: false
    t.integer  "channel_id"
    t.index ["category_asiamerica_id"], name: "sub_category_channel_category_asiamerica_id_bf5dc2db", using: :btree
    t.index ["channel_id"], name: "sub_category_channel_channel_id_ea1963e8", using: :btree
  end

  create_table "technical_report", force: :cascade do |t|
    t.text     "technical_report"
    t.text     "technical_solution"
    t.string   "resolution",         limit: 40
    t.string   "client_contact",     limit: 15
    t.string   "retired",            limit: 15
    t.integer  "return_sale_id"
    t.string   "final_status",       limit: 15
    t.datetime "date_created"
    t.integer  "user_created_id"
    t.index ["return_sale_id"], name: "technical_report_return_sale_id_key", unique: true, using: :btree
    t.index ["user_created_id"], name: "technical_report_user_created_id_55d36641", using: :btree
  end

  create_table "token", id: :integer, default: -> { "nextval('\"Token_id_seq\"'::regclass)" }, force: :cascade do |t|
    t.string   "token_ml",     limit: 200
    t.datetime "date_created",             null: false
  end

  create_table "token_user", force: :cascade do |t|
    t.string  "token",   limit: 300, null: false
    t.integer "user_id",             null: false
    t.index ["user_id"], name: "token_user_user_id_7aabe2e2", using: :btree
  end

  create_table "uploads", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "warehouse", force: :cascade do |t|
    t.string  "name",          limit: 100, null: false
    t.string  "address",       limit: 50,  null: false
    t.string  "phone",         limit: 15,  null: false
    t.string  "email",         limit: 50,  null: false
    t.integer "square_meters"
  end

  create_table "warehouses_stockwarehousecode", force: :cascade do |t|
    t.integer  "initial_stock"
    t.datetime "date_count"
    t.integer  "product_code_id"
    t.integer  "stock_warehouse_id", null: false
    t.index ["product_code_id"], name: "warehouses_stockwarehousecode_product_code_id_d2a12029", using: :btree
    t.index ["stock_warehouse_id"], name: "warehouses_stockwarehousecode_stock_warehouse_id_36e35e62", using: :btree
  end

  add_foreign_key "activity", "channel", name: "activity_channel_id_03d01bf6_fk_channel_id"
  add_foreign_key "answer", "auth_user", column: "user_id", name: "answer_user_id_5d3bcab5_fk_auth_user_id"
  add_foreign_key "answer", "questions", name: "answer_question_id_523c1648_fk_questions_id"
  add_foreign_key "auth_group_permissions", "auth_group", column: "group_id", name: "auth_group_permissions_group_id_b120cbf9_fk_auth_group_id"
  add_foreign_key "auth_group_permissions", "auth_permission", column: "permission_id", name: "auth_group_permissio_permission_id_84c5c92e_fk_auth_perm"
  add_foreign_key "auth_permission", "django_content_type", column: "content_type_id", name: "auth_permission_content_type_id_2f476e4b_fk_django_co"
  add_foreign_key "auth_user_groups", "auth_group", column: "group_id", name: "auth_user_groups_group_id_97559544_fk_auth_group_id"
  add_foreign_key "auth_user_groups", "auth_user", column: "user_id", name: "auth_user_groups_user_id_6a12ed8b_fk_auth_user_id"
  add_foreign_key "auth_user_user_permissions", "auth_permission", column: "permission_id", name: "auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm"
  add_foreign_key "auth_user_user_permissions", "auth_user", column: "user_id", name: "auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id"
  add_foreign_key "category", "channel", name: "categories_category_channel_id_7f77f935_fk_categorie"
  add_foreign_key "channel_commission", "channel", name: "channel_commission_channel_id_53e770e7_fk_channel_id"
  add_foreign_key "channel_commission", "packs", name: "channel_commission_pack_id_a89ecfb4_fk_packs_id"
  add_foreign_key "channel_commission", "product_sale", name: "channel_commission_product_sale_id_cb45b550_fk_product_sale_id"
  add_foreign_key "color_product", "color", name: "color_product_color_id_def2e2d2_fk_color_id"
  add_foreign_key "color_product", "product_sale", column: "product_id", name: "color_product_product_id_4f51680d_fk_product_sale_id"
  add_foreign_key "custom_message_product", "packs", name: "custom_message_product_pack_id_889a505c_fk_packs_id"
  add_foreign_key "custom_message_product", "product_sale", column: "product_id", name: "custom_message_product_product_id_40ada486_fk_product_sale_id"
  add_foreign_key "dimensions_product_sale", "sales", name: "dimensions_product_sale_sale_id_cbd5de10_fk_sales_id"
  add_foreign_key "django_admin_log", "auth_user", column: "user_id", name: "django_admin_log_user_id_c564eba6_fk_auth_user_id"
  add_foreign_key "django_admin_log", "django_content_type", column: "content_type_id", name: "django_admin_log_content_type_id_c4bce8eb_fk_django_co"
  add_foreign_key "inventory", "auth_user", column: "user_charge_id", name: "inventory_user_charge_id_fbd05dbb_fk_auth_user_id"
  add_foreign_key "metrics_mercadolibre", "product", name: "metrics_mercadolibre_product_id_c846974b_fk_product_id"
  add_foreign_key "notifications_specialevent", "channel", name: "notifications_specialevent_channel_id_e52770ec_fk_channel_id"
  add_foreign_key "packs", "category_product_sale", column: "category_pack_id", name: "packs_category_pack_id_eff90d0d_fk_category_product_sale_id"
  add_foreign_key "packs", "channel", name: "packs_channel_id_a0f7b149_fk_channel_id"
  add_foreign_key "packs_products", "color", name: "packs_products_color_id_78cb93c7_fk_color_id"
  add_foreign_key "packs_products", "packs", name: "packs_products_pack_id_d25cb599_fk_packs_id"
  add_foreign_key "packs_products", "product_sale", column: "product_pack_id", name: "packs_products_product_pack_id_e95ec08c_fk_product_sale_id"
  add_foreign_key "price_product", "packs", name: "price_product_pack_id_7a39628f_fk_packs_id"
  add_foreign_key "price_product", "product_sale", name: "price_product_product_sale_id_1afd989c_fk_product_sale_id"
  add_foreign_key "price_variation_pcfactory", "product", name: "products_pricevariat_product_id_fae74b54_fk_product_i"
  add_foreign_key "product", "\"sub-categories\"", column: "sub_category_id", name: "product_sub_category_id_969ff5f9_fk_sub-categories_id"
  add_foreign_key "product", "channel", name: "product_channel_id_bdec9f54_fk_channel_id"
  add_foreign_key "product", "seller", name: "product_seller_id_cb2471f5_fk_seller_id"
  add_foreign_key "product_branch_office", "branch_office", column: "office_id", name: "product_branch_office_office_id_ebe00e24_fk_branch_office_id"
  add_foreign_key "product_branch_office", "product", name: "product_branch_office_product_id_38f3e686_fk_product_id"
  add_foreign_key "product_channel_pivote", "channel", name: "product_channel_pivote_channel_id_1c0dbcd0_fk_channel_id"
  add_foreign_key "product_channel_pivote", "product_sale", column: "product_id", name: "product_channel_pivote_product_id_2d963ecf_fk_product_sale_id"
  add_foreign_key "product_sale", "category_product_sale", name: "product_sale_category_product_sal_a0f02bd6_fk_category_"
  add_foreign_key "products_variation", "product", name: "products_variation_product_id_58e457dc_fk_product_id"
  add_foreign_key "publications_mercadolibre", "seller", name: "notifications_public_seller_id_2019caf8_fk_seller_id"
  add_foreign_key "purchase", "auth_user", column: "user_id", name: "purchase_user_id_7b5a7dd4_fk_auth_user_id"
  add_foreign_key "purchase", "provider", name: "purchase_provider_id_74018c22_fk_provider_id"
  add_foreign_key "purchase", "warehouse", name: "purchase_warehouse_id_f6182a98_fk_warehouse_id"
  add_foreign_key "purchase_product", "product_sale", column: "product_purchase_id", name: "purchase_product_product_purchase_id_a0f8bb9a_fk_product_s"
  add_foreign_key "purchase_product", "purchase", name: "purchase_product_purchase_id_87b8d1e8_fk_purchase_id"
  add_foreign_key "purchase_product", "sales_product_code", column: "product_purchase_code_id", name: "purchase_product_product_purchase_cod_b42e129f_fk_sales_pro"
  add_foreign_key "questions", "channel", name: "questions_channel_id_5ab07d06_fk_channel_id"
  add_foreign_key "questions", "publications_mercadolibre", column: "publication_data_id", name: "questions_publication_data_id_e9b9a966_fk_notificat"
  add_foreign_key "report_activity", "activity", name: "report_activity_activity_id_b02f9fc1_fk_activity_id"
  add_foreign_key "report_activity", "report_supervisor", name: "report_activity_report_supervisor_id_750f7c7f_fk_report_su"
  add_foreign_key "report_supervisor", "auth_user", column: "user_generated_id", name: "report_supervisor_user_generated_id_c04d592b_fk_auth_user_id"
  add_foreign_key "retail-payment", "\"bank-report\"", column: "bank_register_id", name: "retail-payment_bank_register_id_2c634fc9_fk_bank-report_id"
  add_foreign_key "retail-payment", "channel", name: "retail-payment_channel_id_8b1be360_fk_channel_id"
  add_foreign_key "retail-payment-detail", "\"retail-payment\"", column: "retail_payment_id", name: "retail-payment-detai_retail_payment_id_c401df39_fk_retail-pa"
  add_foreign_key "retail-payment-detail", "sales", name: "retail-payment-detail_sale_id_d6c3fa95_fk_sales_id"
  add_foreign_key "retirement", "auth_user", column: "user_created_id", name: "retirement_user_created_id_88030366_fk_auth_user_id"
  add_foreign_key "retirement", "warehouse", name: "retirement_warehouse_id_2aee8750_fk_warehouse_id"
  add_foreign_key "retirement_product", "product_sale", name: "retirement_product_product_sale_id_f9a0816f_fk_product_sale_id"
  add_foreign_key "retirement_product", "retirement", name: "retirement_product_retirement_id_427017b2_fk_retirement_id"
  add_foreign_key "retirement_product", "sales_product_code", column: "product_code_id", name: "retirement_product_product_code_id_b6ecb438_fk_sales_pro"
  add_foreign_key "return", "auth_user", column: "user_id", name: "return_user_id_5fb74454_fk_auth_user_id"
  add_foreign_key "return", "clients_returns", column: "client_id", name: "return_client_id_24850826_fk_clients_returns_id"
  add_foreign_key "return", "courier", name: "return_courier_id_201ffc85_fk_courier_id"
  add_foreign_key "return", "sales", name: "return_sale_id_064626bb_fk_sales_id"
  add_foreign_key "return_product", "product_sale", column: "product_sale_return_id", name: "return_product_product_sale_return__e8699c04_fk_product_s"
  add_foreign_key "return_product", "return", column: "return_sale_id", name: "return_product_return_sale_id_3448c15d_fk_return_id"
  add_foreign_key "return_product", "sales_product_code", column: "product_sale_code_return_id", name: "return_product_product_sale_code_re_1ce33ad4_fk_sales_pro"
  add_foreign_key "robot_aceptance", "questions", column: "question_asociated_id", name: "robot_aceptance_question_asociated_id_40088263_fk_questions_id"
  add_foreign_key "robot_aceptance", "robot_response", column: "response_id", name: "robot_aceptance_response_id_4d411e5d_fk_robot_response_id"
  add_foreign_key "robot_intention_words", "robot_intention", column: "intention_id", name: "robot_intention_word_intention_id_ab92c539_fk_robot_int"
  add_foreign_key "robot_intention_words", "robot_words", column: "word_id", name: "robot_intention_words_word_id_4e78e359_fk_robot_words_id"
  add_foreign_key "robot_response", "robot_intention", column: "intention_id", name: "robot_response_intention_id_1c55d108_fk_robot_intention_id"
  add_foreign_key "robot_synonyms", "robot_words", column: "word_id", name: "robot_synonyms_word_id_6eceeb3a_fk_robot_words_id"
  add_foreign_key "robot_words_publication_asociated", "publications_mercadolibre", column: "publicationdatamercadolibre_id", name: "robot_words_publicat_publicationdatamerca_367337eb_fk_publicati"
  add_foreign_key "robot_words_publication_asociated", "robot_words", column: "word_id", name: "robot_words_publicat_word_id_cafff071_fk_robot_wor"
  add_foreign_key "robot_wrong_words", "robot_synonyms", column: "synonym_id", name: "robot_wrong_words_synonym_id_e893c985_fk_robot_synonyms_id"
  add_foreign_key "robot_wrong_words", "robot_words", column: "word_id", name: "robot_wrong_words_word_id_5689dac0_fk_robot_words_id"
  add_foreign_key "sale_product_commission", "sale_product_pivote", column: "sale_product_id", name: "sale_product_commiss_sale_product_id_ff0a176c_fk_sale_prod"
  add_foreign_key "sale_product_pivote", "packs", column: "from_pack_id", name: "sale_product_pivote_from_pack_id_c09e8413_fk_packs_id"
  add_foreign_key "sale_product_pivote", "product_sale", column: "product_id", name: "sale_product_pivote_product_id_61d605ea_fk_product_sale_id"
  add_foreign_key "sale_product_pivote", "sales", name: "sale_product_pivote_sale_id_d82bb353_fk_sales_id"
  add_foreign_key "sale_product_pivote", "sales_product_code", column: "product_code_id", name: "sale_product_pivote_product_code_id_2408f0ac_fk_sales_pro"
  add_foreign_key "sales", "auth_user", column: "last_user_update_id", name: "sales_last_user_update_id_81028058_fk_auth_user_id"
  add_foreign_key "sales", "auth_user", column: "user_id", name: "sales_user_id_894ff46a_fk_auth_user_id"
  add_foreign_key "sales", "channel", name: "sales_channel_id_bd881a60_fk_channel_id"
  add_foreign_key "sales", "sales_report", name: "sales_sales_report_id_c0db2bff_fk_sales_report_id"
  add_foreign_key "sales_product_code", "auth_user", column: "user_reset_id", name: "sales_product_code_user_reset_id_417bc1c1_fk_auth_user_id"
  add_foreign_key "sales_product_code", "color", name: "sales_product_code_color_id_11d79572_fk_color_id"
  add_foreign_key "sales_product_code", "product_sale", column: "product_id", name: "sales_product_code_product_id_0aad3e9b_fk_product_sale_id"
  add_foreign_key "sales_report", "auth_user", column: "user_created_id", name: "sales_sales_report_user_created_id_99bc84fb_fk_auth_user_id"
  add_foreign_key "seller_variation", "seller", name: "seller_variation_seller_id_74d3276c_fk_seller_id"
  add_foreign_key "sku_category_channel", "packs", name: "sku_category_channel_pack_id_0dd2d37f_fk_packs_id"
  add_foreign_key "sku_category_channel", "product_sale", name: "sku_category_channel_product_sale_id_68897523_fk_product_s"
  add_foreign_key "sku_category_channel", "sub_category_channel", name: "sku_category_channel_sub_category_channel_4c11998f_fk_sub_categ"
  add_foreign_key "sku_ripley", "channel", name: "sku_ripley_channel_id_3cc765c8_fk_channel_id"
  add_foreign_key "sku_ripley", "color", name: "sku_ripley_color_id_b0ce3c2b_fk_color_id"
  add_foreign_key "sku_ripley", "packs", column: "pack_ripley_id", name: "sku_ripley_pack_ripley_id_70c41e19_fk_packs_id"
  add_foreign_key "sku_ripley", "product_sale", column: "product_ripley_id", name: "sku_ripley_product_ripley_id_34f57b91_fk_product_sale_id"
  add_foreign_key "stock_inventory_product", "inventory", name: "stock_inventory_product_inventory_id_5e4fcc86_fk_inventory_id"
  add_foreign_key "stock_inventory_product", "product_sale", column: "product_id", name: "stock_inventory_product_product_id_005ebb2a_fk_product_sale_id"
  add_foreign_key "stock_inventory_product", "sales_product_code", column: "product_code_id", name: "stock_inventory_prod_product_code_id_3ca4eefb_fk_sales_pro"
  add_foreign_key "stock_warehouse", "product_sale", name: "stock_warehouse_product_sale_id_10d4ce65_fk_product_sale_id"
  add_foreign_key "stock_warehouse", "warehouse", name: "stock_warehouse_warehouse_id_2e72d93a_fk_warehouse_id"
  add_foreign_key "sub-categories", "category", name: "categories_subcatego_category_id_5056b671_fk_categorie"
  add_foreign_key "sub_category_channel", "category_product_sale", column: "category_asiamerica_id", name: "sub_category_channel_category_asiamerica__bf5dc2db_fk_category_"
  add_foreign_key "sub_category_channel", "channel", name: "sub_category_channel_channel_id_ea1963e8_fk_channel_id"
  add_foreign_key "technical_report", "auth_user", column: "user_created_id", name: "technical_report_user_created_id_55d36641_fk_auth_user_id"
  add_foreign_key "technical_report", "return", column: "return_sale_id", name: "technical_report_return_sale_id_0c441328_fk_return_id"
  add_foreign_key "token_user", "auth_user", column: "user_id", name: "token_user_user_id_7aabe2e2_fk_auth_user_id"
  add_foreign_key "warehouses_stockwarehousecode", "sales_product_code", column: "product_code_id", name: "warehouses_stockware_product_code_id_d2a12029_fk_sales_pro"
  add_foreign_key "warehouses_stockwarehousecode", "stock_warehouse", name: "warehouses_stockware_stock_warehouse_id_36e35e62_fk_stock_war"
end
